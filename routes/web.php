<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/register', function(){
    return view('auth.admin-register');
});
Route::post('admin/register', "Auth\RegisterController@register")->name('admin-register');

Route::middleware('auth')->group(function () {
    Route::get('/add-product', "ProductController@addProduct")->middleware('role:admin')->name('add-product');
    Route::get('/edit-product/{product}', "ProductController@getProduct")->middleware('role:admin')->name('edit-product');
    Route::post('/update-product/{product}', "ProductController@updateProduct")->middleware('role:admin')->name('update-product');
    Route::post('/store-product', "ProductController@storeProduct")->middleware('role:admin')->name('store-product');
    Route::get('/list', "ProductController@list")->name('list');
    Route::post('/update-status/{product}', "ProductController@decativeProduct")->name('update-status');
    Route::post('/delete-product/{product}', "ProductController@deleteProduct")->name('delete-product');
});
