<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\AddProduct;
use App\Http\Requests\EditProduct;

class ProductController extends Controller
{
    public function list(){
        $role=request()->user()->role;
        if($role=='admin'){
            $products=Product::paginate(5);
        }else 
        $products=Product::where('status', 'active')->paginate(5);
        return view('list', ['products'=>$products, 'role'=>$role]);
    }
    public function getProduct(Product $product){
        return view('edit-product',['product'=>$product]);
    }
    public function addProduct()
    {   
        return view('add-product');
    }
    public function storeProduct(AddProduct $req)
    {
        $validated = $req->validated();
        $product=Product::create([
            'name'=>$validated['name'],
            'price'=>$validated['price'],
            'description'=>$validated['desc'],
            'discount'=>$validated['discount']??NULL,
        ]);
        $storage_path="public";
        $path=$req->file('image')->storeAs($storage_path, $product->id."_".$req->image->getClientOriginalName());
        $product->image=basename($path);
        $product->save();
        return back()->with('success', "Product added");
    }

    public function updateProduct(Product $product, EditProduct $req)
    {
        $validated = $req->validated();
        $product->update([
            'name'=>$validated['name'],
            'price'=>$validated['price'],
            'description'=>$validated['desc'],
            'discount'=>$validated['discount']??NULL,
        ]);
        if($req->file('image')){
            $storage_path="public";
            $path=$req->file('image')->storeAs($storage_path, $product->id."_".$req->image->getClientOriginalName());
            $product->image=basename($path);
            $product->save();
        }
        return back()->with('success', "Product updated");
    }

    public function decativeProduct(Product $product) {
        $product->status="deactive";
        $product->save();
        return response()->json(["message"=>"Product is updated"],200);
    }

    public function deleteProduct(Product $product){
        $product->delete();
        return response()->json(["message"=>"Product is deleted"],200);
    }
}
