@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="row">
            @forelse($products as $product)
            <div class="col-md-4">
                <div class="card">
                    <img src="{{asset(($product->image))}}" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4><b>{{$product->name}}</b></h4>
                        <p>{{$product->description}}</p>
                        <p>Rs.{{$product->price}}</p>
                        <p>Discount: {{$product->discount??"0"}}%</p>
                        @if($role=='admin')
                        <div class="btn btn-primary"><a style="color:#000" href="{{route('edit-product',$product->id)}}">Edit</a></div>
                        @if($product->status=='active')
                        <button class="btn btn-default" onclick="disableProduct({{$product->id}})">Deactive</button>
                        @endif
                        <button class="btn btn-danger" onclick="deleteProduct({{$product->id}})">Delete</button>
                        @endif
                    </div>
                </div>
            </div>
            @empty
            No product found
            @endforelse
            {{$products->links()}}
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
function disableProduct(product){
  $.ajax({
      url:"{{url('update-status')}}/"+product,
      method:'POST',
      headers:{
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      error:function () {},
      success:function (data) {
        alert("Product is deactived")
      }
	});
}
function deleteProduct(product){
  $.ajax({
      url:"{{url('delete-product')}}/"+product,
      method:'POST',
      headers:{
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      error:function () {},
      success:function (data) {
        alert("Product is deleted")
      }
	});
}
</script>
@endsection