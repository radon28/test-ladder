@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Product
                <a class="pull-right" href="{{route('list')}}">List</a>
                </div>
                <div class="card-body">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <ul class="list-unstyled">
                        <li>{{ Session::get('success') }}</li>
                    </ul>
                </div>
                @endif
                @if (Session::has('errors'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <ul class="list-unstyled">
                    @foreach (Session::get('errors')->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{route('update-product',$product->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="row">
                        <div class='form-group col-sm-4'>
                            <label for="name">Product Name*</label>
                            <input name="name" class="form-control" type="text" value="{{old('name')?? $product->name}}" required/>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='form-group col-sm-4'>
                            <label for="price">Price*</label>
                            <input name="price" class="form-control" type="text" value="{{old('price')?? $product->price}}" required/>
                            @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='form-group col-sm-4'>
                            <label for="image">image*</label>
                            <input name="image" class="form-control" type="file"/>
                            @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='form-group col-sm-4'>
                            <label for="discount">discount</label>
                            <input name="discount" class="form-control" type="text" value="{{old('discount')?? $product->discount}}"/>
                            @error('discount')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='form-group col-sm-4'>
                            <label for="desc">Description*</label>
                            <textarea name="desc" class="form-control">{{old('desc')?? $product->description}}</textarea>
                            @error('desc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='form-group col-sm-4'>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
